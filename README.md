# Ehren Murdick's Config
Cleaned up by [Jessica Lynn Suttles](http://github.com/jlsuttles/config)

## Install everything:

* `rake install:all`

## Or specific stuff:

* `rake install:ack`
* `rake install:git`
* `rake install:irb`
* `rake install:vim`
* `rake install:zsh`
